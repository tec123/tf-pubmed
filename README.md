# PubMed
![Books](images/books.jpg)

## Intro
PubMed is a search engine to access a database of abstracts on medical research. It's a very popular website for a lot of life sciences and biomedical topics. From 1971 to 1997, accessing this database was only available through institutional facilities such as university libraries. 

Most studies in this database starts with abstract. For many medical researchers, these abstracts are hard to read because they were not written in easy to comprehend structure. In order for a researcher to search this database for relevant study, they need to read tens of these abstracts until they find the most suitable match. However, the complex structure of these abstracts might discourage them from reading adequate number of abstracts to find the best match.

In the paper [PubMed 200k RCT](https://arxiv.org/abs/1710.06071), the authors (Dernoncourt, Lee) created a 200000 abstracts of randomized controlled trials (RCT) dataset, based on PubMed for sequential sentence classification. 

## Goal
Our goal is to reformat PubMed database abstracts to enable researchers to glance at them quickly and identify the study that best match what they are looking for. We will build a system that can automatically rewrite a complex medical publication abstracts into easy to read format. We will use a DistilBert Model transformer (HuggingFace) with a sequence classification head on top. To build our system, we’ll use “PubMed 200k RCT” dataset. This dataset contains five basic labels: background, objective, method, result or conclusion. Given a medical abstract, our task will be to train a model that can classify it into one of these labels.

<!--- ## Tasks -->

<!--- ## Results  -->

## License
This project is licensed under the GNU GPLv3 License